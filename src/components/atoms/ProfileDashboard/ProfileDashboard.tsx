import React from "react";
import { Users } from "../../../model";
import { formatDate } from "../../../utils";
import './ProfileDashboard.scss';

export default function ProfileDashboard({...props}: Users){
    const {name, nip, lastLoginDateDashboard } = props;

    return(
        <div className="profile-dash-wrapper">
            <p className='profile-user'>
                {name} / {nip}
            </p>
            <p className='last-login'>
                {lastLoginDateDashboard != null && 
                    <span>
                        Last Login {formatDate(lastLoginDateDashboard)}
                    </span>
                }
                {lastLoginDateDashboard == null &&
                    <span>
                        You are first login
                    </span>
                }
            </p>
        </div>
    );
}