import classNames from "classnames";
import { Badge } from "primereact/badge";
import { Ripple } from "primereact/ripple";
import React, { useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { AppMenuProps } from "../../../model";
import { CSSTransition } from 'react-transition-group';
import './SubMenu.scss';

export default function SubMenu({model, onMenuItemClick, root}: AppMenuProps){
    const [activeIndex, setActiveIndex] = useState<any[]>([]);
    const locRoute = useLocation();

    const onHandleMenuItemClick = (event: any, item: any, index: any) => {
        //avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        //execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }
        
        if (activeIndex.find(x => x === index))
            setActiveIndex((state) => state.filter(x => x !== index));
        else
            setActiveIndex((state) => [...state, index]);

        if (onMenuItemClick) {
            onMenuItemClick({
                originalEvent: event,
                item: item
            });
        }
    }

    const onKeyDown = (event: any) => {
        if (event.code === 'Enter' || event.code === 'Space'){
            event.preventDefault();
            event.target.click();
        }
    }
    
    const renderLinkContent = (item: any) => {
        let submenuIcon = item.items && <i className="pi pi-fw pi-angle-down menuitem-toggle-icon"></i>;
        let badge = item.badge && <Badge value={item.badge} />

        return (
            <React.Fragment>
                <i className={item.icon}></i>
                <span>{item.label}</span>
                {submenuIcon}
                {badge}
                <Ripple/>
            </React.Fragment>
        );
    }
    
    const renderLink = (item: any, i: any) => {
        let content = renderLinkContent(item);

        if (item.to) {
            return (
                <NavLink aria-label={item.label} onKeyDown={onKeyDown} role="menuitem" className={({isActive}) => (isActive ? 'p-ripple router-link-active router-link-exact-active' : 'p-ripple')} to={item.to} onClick={(e) => onHandleMenuItemClick(e, item, i)} target={item?.target}>
                    {content}
                </NavLink>
            )
        }
        else {
            let actIndex: number = 0;
            let act = locRoute.pathname; 
            let active = act.split('/').length === 2 ? `/${act.split('/')[1]}` : `/${act.split('/')[1]}/${act.split('/')[2]}`;
            
            if(item?.items && item?.items !== null){
                let actItems = item?.items as any[];
                let routeActive = actItems.find(x => x?.to === active);
                if(routeActive){
                    actIndex = i;
                }
            }

            let styleClass = classNames('p-ripple', {
                'router-link-active': actIndex === i,
                'router-link-exact-active': actIndex === i
            });

            return (
                <a tabIndex={0} aria-label={item.label} onKeyDown={onKeyDown} role="menuitem" href={item?.url} className={styleClass} onClick={(e) => onHandleMenuItemClick(e, item, i)} target={item?.target}>
                    {content}
                </a>
            );
        }
    }

    let items = model && model.map((item, i) => {
        
        let active = activeIndex.find(x => x === i) ? true : false;

        let styleClass = classNames('', {
            'layout-menuitem-category': root,
            'active-menuitem': active && item?.items
        });

        if(root){
            return (
                <li className={styleClass} key={i} role="none">
                    {root === true && <React.Fragment>
                        <div className="layout-menuitem-root-text" aria-label={item.label}>{item.label}</div>
                        <SubMenu model={item?.items} onMenuItemClick={onMenuItemClick} />
                    </React.Fragment>}
                </li>
            );
        }else{
            return (
                <li className={styleClass} key={i} role="none"> 
                    {renderLink(item, i)}
                    <CSSTransition classNames="layout-submenu-wrapper" timeout={{ enter: 1000, exit: 450 }} in={active} unmountOnExit>
                        <SubMenu model={item?.items} onMenuItemClick={onMenuItemClick} />
                    </CSSTransition>
                </li>
            );
        }

    });

    return items ? <ul className="layout-menu">{items}</ul> : null;
}   