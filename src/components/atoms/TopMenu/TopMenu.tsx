import React from "react";
import { TopMenus } from "../../../model";
import { CSSTransition } from 'react-transition-group';
import './TopMenu.scss';
import classNames from "classnames";
import { Button } from 'primereact/button';

type widthMenu = 'short' | 'normal' | 'long';

type TopMenuProps = {
    model: Array<TopMenus>
    showIn: boolean
    message?: string
    widthMenu?: widthMenu
    line?: boolean
    showAll?: boolean
}

export default function TopMenu({model, showIn, message, widthMenu, line, showAll}: TopMenuProps){
    const onMenuClick = (ev: any, command: any) => {
        if(command){
            command(ev);
        }
        ev.preventDefault();
    }

    const ulWrapperClass = classNames('ul-top-menu', {
        'ul-width-short': widthMenu === 'short',
        'ul-width-long': widthMenu === 'long',
        'list-select-line': line === true
    });
    
    return(
        <div className="top-menu-wrapper">
            <CSSTransition classNames="my-notif-anim" timeout={{ enter: 200, exit: 20 }} in={showIn} unmountOnExit>
                <ul className={ulWrapperClass}>
                    {model.map((item, i) =>
                        <li className="li-select-textmenu" key={i}>
                            {item?.title && item?.subTitle &&
                                <p className="p-title-menu">
                                    <span className="span-title">{item?.title}</span>
                                    <span className="span-subtitle">{item?.subTitle}</span>  
                                </p>
                            }
                            <a href='#/' onClick={ev => onMenuClick(ev, item?.command)}>
                                {item?.icon && <i className={`pi ${item?.icon}`}></i>}
                                <span>{item?.label}</span>
                            </a>
                        </li>
                    )}
                    {model.length === 0 && message &&
                        <li className="li-message-empty">
                            <span>{message}</span>
                        </li>
                    }
                    {showAll === true && model.length > 0 && 
                        <div className="btn-show-all-menu">
                            <Button label="Show All" className="p-button-text p-button-sm" />
                        </div>
                    }
                </ul>
            </CSSTransition>
        </div>
    );
} 