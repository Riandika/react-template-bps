import React from "react";
import './UserInfo.scss';

type UserInfoProps = {
    name: any
    nip: any
}

export default function UserInfo({name, nip}: UserInfoProps){
    return(
        <div className='user-info-wrapper'>
            <p className='p1'>{name}</p>
            <p className='p2'>{nip}</p>
        </div>
    );
}