import React from "react";
import './CardDashboard.scss';
import { Card } from "primereact/card";
import { ProgressBar } from "primereact/progressbar";

export default function CardDashboard({...props}){
    const footer = (
        <div className="footer-nilai">
            <p className="nilai-p1">Total Nilai</p>
            <p className="nilai-p2">Rp 247,076,391,622.50</p>
            <ProgressBar value={100} showValue={false} mode="determinate" style={{ height: '5px', marginTop: '.4rem', marginBottom: '.2rem' }} color="var(--yellow-500)"></ProgressBar>
        </div>
    );

    return(
        <div className="card-dashboard-wrapper">
            <Card className="card-dash" title="Waiting Response Host" footer={footer}>
                <div className="content-trx">
                    <p className="trx-p1">Total Jumlah Transaksi</p>
                    <p className="trx-p2">{props.num}</p>
                </div>
            </Card>
        </div>
    );
}