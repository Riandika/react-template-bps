import React from "react";
import './ButtonCustom.scss';
import classNames from 'classnames';

type shapeButtonCusType = 'circle' | 'square';

export interface ButtonCusProps extends Omit<React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement>, 'ref'> {
    icon?: string;
    shape?: shapeButtonCusType;
    children?: React.ReactNode;
}

export default function ButtonCustom(props: ButtonCusProps){
    const wrapperClass = classNames(`layout-topbar-button p-link ${props?.className}`, {
        'topbar-btn-shape-square': props.shape === 'square'
    });

    return(
        <button type="button" {...props} className={wrapperClass}>
            {props.children == null && <i className={`pi ${props?.icon}`}/>}
            {props.children != null && props.children }
        </button>
    );
}