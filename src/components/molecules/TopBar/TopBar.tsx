import React from 'react';
import { Logo } from '../../../assets';
import './TopBar.scss';
import { Badge } from 'primereact/badge';
import { Avatar } from 'primereact/avatar';
import { MyButton, TopMenu, UserInfo } from '../../atoms';
import { useAuthRedux, useMainRedux } from '../../../config';

export default function TopBar(){
    const { notifItems, notifMsgEmpty, onNotificationClick, isNotifTopBar, profileItems, isProfileTopBar, onProfileClick, onToggleMenuClick } = useMainRedux();
    const { user } = useAuthRedux();
    const { name, nip } = user;
    
    return(
        <div className="top-bar-wrapper">
            <MyButton icon='pi-bars' shape='square' onClick={onToggleMenuClick} />

            <div className="top-bar-logo">
                <img className='logo-img' src={Logo} alt="logo" />
            </div>
            
            <div className='top-bar-right'>
                <div className='top-notif-layout'>
                    <MyButton icon='pi-bell' shape='square' onClick={onNotificationClick} />
                    {notifItems.length > 0 && <Badge className='badge-notif' severity="danger" />}
                    <TopMenu model={notifItems} showIn={isNotifTopBar} message={notifMsgEmpty} widthMenu="long" line={true} showAll={true} />
                </div>

                <div className='pipe-line'></div>

                <div className='profile-user'>
                    <div className='profile-user-btn'>
                        <MyButton shape='square' onClick={onProfileClick}>
                            <Avatar icon='pi pi-user' className="user-avatar-top" size="normal" shape="square" />
                            <UserInfo name={name} nip={nip} />
                        </MyButton>
                    </div>
                    <TopMenu model={profileItems} showIn={isProfileTopBar} widthMenu="short" />
                </div>
            </div>
        </div>
    );
}