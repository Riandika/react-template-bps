import React from 'react';
import './AppFooter.scss';

type FooterProps = {
    footerText: string
}

export default function AppFooter(props: FooterProps){
    return(
        <div className="footer-wrapper">
            <p className='footer-text'>{props.footerText}</p>
        </div>
    );
}