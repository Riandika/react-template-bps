import React from "react";
import { useMainRedux } from "../../../config";
import { SetMainInitial } from "../../../utils";
import { SubMenu } from "../../atoms";
import './AppMenu.scss';

export default function AppMenu(){
    const { menu } = SetMainInitial();
    const { onMenuItemClick } = useMainRedux();

    return(
        <div className="app-menu-wrapper">
            <div className="top-app-menu">
                BPS 2.1.17
            </div>
            <div className="app-menu-items">
                <SubMenu model={menu} onMenuItemClick={onMenuItemClick} root={false} />
            </div>
        </div>
    );
}