import React from "react";
import { Panel } from 'primereact/panel';
import './PanelDashboard.scss';
import { CardDashboard } from "../../atoms";
import { useMainRedux } from "../../../config";
import classNames from "classnames";

export default function PanelDashboard({...props}){
    const { staticMenuInactive, layoutMode } = useMainRedux();
    
    const template = (options: any) => {
        const toggleIcon = options.collapsed ? 'pi pi-chevron-down' : 'pi pi-chevron-up';
        const className = `panel-header-custom ${options.className} border-none bg-transparent mt-3 mb-0 pt-0 pb-0 justify-content-start`;
        const titleClassName = `${options.titleClassName} pr-2`;

        return (
            <div className={className}>
                <span className={titleClassName}>
                    GL to GL
                </span>
                <button className={`${options.togglerClassName} btn-togle-panel`} onClick={options.onTogglerClick}>
                    <span className={toggleIcon}></span>
                </button>
            </div>
        )
    }

    const cardLayoutStyle = classNames('body-card-layout', {
        'body-card': staticMenuInactive && layoutMode === 'static',
        'body-card-open': !staticMenuInactive && layoutMode === 'static'
    });

    return (
        <div className="panel-dashboard-wrapper">
            <Panel className="panel-body-custom" headerTemplate={template} toggleable collapsed={false}>
                <div className={cardLayoutStyle}>
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                </div>
                <div className="line-panel-dash"></div>
            </Panel>
            <Panel className="panel-body-custom" headerTemplate={template} toggleable collapsed={false}>
                <div className={cardLayoutStyle}>
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                    <CardDashboard {...props} />
                </div>
            </Panel>
        </div>
    );
}