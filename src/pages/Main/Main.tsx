import React, { useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import { AppFooter, AppMenu, TopBar } from '../../components';
import classNames from 'classnames';
import PrimeReact from 'primereact/api';
import { CSSTransition } from 'react-transition-group';
import { configMain, MainEnv, useMainRedux } from '../../config';
import './Main.scss';
import '../../assets/styles/layout.scss';
import { SetMainInitial, SetMainInterval } from '../../utils';
import { ConfirmDialog } from 'primereact/confirmdialog';

export default function Main() {
    PrimeReact.ripple = true;
    
    const {
        layoutMode,
        ripple,
        staticMenuInactive, 
        overlayMenuActive,
        mobileMenuActive,
        onSidebarClick,
        onWrapperClick
    } = useMainRedux();

    const { getMainInterval } = SetMainInterval();
    const { getMainProfileItems } = SetMainInitial();
    const { isTablet } = MainEnv(); 
    const { confirmDialogKey } = configMain;

    const [tablet, setTablet] = useState(false);

    useEffect(() => {
        setTablet(isTablet());

        window.addEventListener('resize', () => setTablet(isTablet()));

        if (mobileMenuActive) {
            addClass(document.body, "body-overflow-hidden");
        } else {
            removeClass(document.body, "body-overflow-hidden");
        }

        return () => {
            window.removeEventListener('resize', () => setTablet(isTablet()));
        }
    }, [mobileMenuActive, isTablet]);

    useEffect(() => {
        getMainProfileItems(tablet);
    }, [getMainProfileItems, tablet]);

    useEffect(() => {
        getMainInterval();
    }, [getMainInterval]);
    
    const addClass = (element: any, className: any) => {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    }

    const removeClass = (element: any, className: any) => {
        if (element.classList)
            element.classList.remove(className);
        else
            element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }

    const wrapperClass = classNames('layout-wrapper', {
        'layout-overlay': layoutMode === 'overlay',
        'layout-static': layoutMode === 'static',
        'layout-static-sidebar-inactive': staticMenuInactive && layoutMode === 'static',
        'layout-overlay-sidebar-active': overlayMenuActive && layoutMode === 'overlay',
        'layout-mobile-sidebar-active': mobileMenuActive,
        'p-ripple-disabled': ripple === false
    });
    
    return(
        <div className={wrapperClass} onClick={onWrapperClick}>
            <ConfirmDialog breakpoints={{'960px': '55vw', '640px': '100vw'}} style={{width: '50vw'}} tagKey={confirmDialogKey.mainKey} />
            
            <TopBar />

            <div className='layout-sidebar' onClick={onSidebarClick}>
                <AppMenu />
            </div>

            <div className='layout-main-container'>
                <div className='layout-main'>
                    <Outlet />
                </div>
                <AppFooter footerText="© 2022 PT Bank Mandiri (Persero) Tbk." />
            </div>

            <CSSTransition classNames="layout-mask" timeout={{ enter: 150, exit: 150 }} in={mobileMenuActive} unmountOnExit>
                <div className="p-component-overlay"></div>
            </CSSTransition>

        </div>
    );
}