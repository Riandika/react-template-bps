import Login from "./Login/Login"
import Main from "./Main/Main"
import Home from "./Home/Home"

export {
    Login,
    Main,
    Home
}