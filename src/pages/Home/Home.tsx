import React, { useEffect, useState } from 'react';
import { PanelDashboard, ProfileDashboard } from '../../components';
import { useAuthRedux } from '../../config';
import './Home.scss';
import { SelectButton, SelectButtonChangeParams } from 'primereact/selectbutton';

export default function Home(){
    const { user } = useAuthRedux();
    const [valueTogle, setValueTogle] = useState('All');
    const [coba, setCoba] = useState({num: '', status: ''});

    const paymentOptions = [
        {name: 'All', value: 'All'},
        {name: 'Rekonsiliasi', value: 'Rekonsiliasi'},
        {name: 'Dispute', value: 'Dispute'}
    ];

    useEffect(() => {
        switch (valueTogle) {
            case 'All':
                setCoba({
                    num: '1',
                    status: 'All'
                });
                
                break;
            case 'Rekonsiliasi':
                setCoba({
                    num: '2',
                    status: 'Rekonsiliasi'
                });

                break;
            case 'Dispute':
                setCoba({
                    num: '3',
                    status: 'Dispute'
                });

                break;
        
            default:
                break;
        }
    }, [valueTogle]);

    const setTogleDashboard = (ev: SelectButtonChangeParams) => {
        setValueTogle(ev.value);
        ev.preventDefault();
    }

    return(
        <div className="home-page-wrapper">
            <div className='profile-list'>
                <ProfileDashboard {...user} />
            </div>
            <div className='toogle-dashboard'>
                <SelectButton value={valueTogle} options={paymentOptions} onChange={(e) => setTogleDashboard(e)} unselectable={false} optionLabel="name" className='select-button-dash' />
            </div>
            <div className='panel-dashboard'>
                <PanelDashboard {...coba} />
            </div>

        </div>
    );
}
