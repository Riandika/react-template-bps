import React from 'react';
import './App.scss';
import { GlobalProvider, Routes } from '../config';

function App() {
  return (
    <GlobalProvider>
      <Routes />
    </GlobalProvider>
  );
}

export default App;
