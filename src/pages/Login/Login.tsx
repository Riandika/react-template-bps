import React, { useState } from 'react';
import { Field, Form } from 'react-final-form';
import './Login.scss';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Card } from 'primereact/card';
import { Message } from 'primereact/message';
import { classNames } from 'primereact/utils';
import { CSSTransition } from 'react-transition-group';
import { Logo } from '../../assets';
import { useNavigate } from 'react-router-dom';

type LoginProps = {
    onSuccessLogin: () => void
}

export default function Login({onSuccessLogin}: LoginProps){
    const [passwordType, setPasswordType] = useState('password');
    const [maskIconPwd, setMaskIconPwd] = useState('pi pi-eye');
    const [errorLogin, setErrorLogin] = useState(false);
    const [errorMsg, setErrorMsg] =useState(' ');    
    const [loginState, setLoginState] = useState(false);

    const nav = useNavigate();
    const isFormFieldValid = (meta: any) => !!(meta.touched && meta.error);
    const getFormErrorMessage = (meta: any) => {
        return isFormFieldValid(meta) && <small className="p-error">{meta.error}</small>;
    };

    const [wrongPwd, setWrongPwd] =useState(0);

    const validate = (data: any) => {
        let errors = {} as any;

        if (!data.username || !String(data.username).trim()) {
            errors.username = 'Username is required.';
        }

        if (!data.password || !String(data.password).trim()) {
            errors.password = 'Password is required.';
        }

        return errors;
    };

    const onSubmit = async (data: any, form: any) => {
        setLoginState(true);

        setTimeout(async () => {
            if(data?.username === 'admin' && data?.password === 'admin'){
                setTimeout(() => {
                    onSuccessLogin();
                }, 300);
                nav('/', {replace: false});
                form.restart();
                setWrongPwd(0);
                setErrorLogin(false);
                setLoginState(false);
            }else{
                setWrongPwd(wrongPwd + 1);
                let wrongPassword = await getState(setWrongPwd);
                let msg = '';
                msg = 'Invalid Username/Password. Silahkan coba kembali. Akun akan terkunci apabila gagal login 3 kali';
                if(wrongPassword > 2){
                    msg = 'Anda terlocked out dari LDAP';
                }
                setErrorMsg(msg); 
                setErrorLogin(true);  
                setLoginState(false);  
            }
        }, 800);
    };

    const getState = (
        setState: React.Dispatch<React.SetStateAction<any>>
      ): Promise<any> => {
        return new Promise((resolve) => {
          setState((currentState: any) => {
            resolve(currentState);
            return currentState;
          });
        });
    }

    const setMaskPassword = () => {
        if(passwordType === 'password'){
            setMaskIconPwd('pi pi-eye-slash');
            setPasswordType('text');
            
            return;
        }

        setMaskIconPwd('pi pi-eye');
        setPasswordType('password');
    }

    return(
        <div className='login-page-wrapper'>
            <Card className='card'>
                <div className='logo-layout'>
                    <img className='logo' src={Logo} alt="logo" />
                </div>
                <Form onSubmit={onSubmit} initialValues={{ username: '', password: '' }} validate={validate} render={({ handleSubmit }) => (
                    <form onSubmit={handleSubmit} className="p-fluid">
                        <Field name="username" render={({ input, meta }) => (
                            <div className="field">
                                <span className="p-float-label p-input-icon-left">
                                    <i className="pi pi-user" />
                                    <InputText id="username" autoComplete="off" {...input} className={classNames({ 'p-invalid': isFormFieldValid(meta) })} />
                                    <label htmlFor="username" className={classNames({ 'p-error': isFormFieldValid(meta) })}>Username*</label>
                                </span>
                                {getFormErrorMessage(meta)}
                            </div>
                        )} />

                        <Field name="password" render={({ input, meta }) => (
                            <div className="field">
                                <span className="p-float-label p-input-icon-left p-input-icon-right">
                                    <i className="pi pi-lock" />
                                    <InputText id='password' type={passwordType} {...input} className={classNames({ 'p-invalid': isFormFieldValid(meta) })} />
                                    <label htmlFor="password" className={classNames({ 'p-error': isFormFieldValid(meta) })}>Password*</label>
                                    <i className={maskIconPwd + ' cursor-pointer'} onClick={setMaskPassword} />
                                </span>
                                {getFormErrorMessage(meta)}

                            </div>
                        )} />
                        
                        <div className='error-message'>
                            <CSSTransition classNames="my-message" timeout={{ enter: 200, exit: 100 }} in={errorLogin} unmountOnExit>
                                <Message className='msg' 
                                severity = "error"
                                content = {errorMsg}/>
                            </CSSTransition>
                        </div>
                        
                        <div className='login-button'>
                            <Button type="submit" label="Masuk" className="mt-2 p-button-sm p-button-primary" icon="pi pi-sign-in" iconPos="right" loading={loginState} />
                        </div>
                    </form>
                )} />
            </Card>
        </div>
    );
}
