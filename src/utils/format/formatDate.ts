const DATE_FORMATTER = new Intl.DateTimeFormat('ID', {
    year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit'
});

export function formatDate(date: any){
    return DATE_FORMATTER.format(date);
}