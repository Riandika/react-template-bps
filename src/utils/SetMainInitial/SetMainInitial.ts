import { useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { configMain, useMainRedux } from "../../config";
import { MenuModel } from "../../model";
import { confirmDialog } from 'primereact/confirmdialog';

export default function SetMainInitial(){
    const { setProfileItems } = useMainRedux();
    const { confirmDialogKey } = configMain;

    const nav = useNavigate();

    const menu: MenuModel[] = [
        {
            label: 'Dashboard',
            to: '/home'
        },
        {
            label: 'GL to GL',
            to: '/gl-to-gl'
        },
        {
            label: 'GL to Account',
            to: '/gl-to-account'
        },
        {
            label: 'Account to Account',
            to: '/account-to-account'
        },
        {
            label: 'Approval',
            items: [
                {label: 'GL to GL', to: '/approval/gl-to-gl'},
                {label: 'GL to Account', to: '/approval/gl-to-account'},
                {label: 'Account to Account', to: '/approval/account-to-account'}
            ]
        },
        {
            label: 'Approval Accounting',
            to: '/approval/accounting'
        },
        {
            label: 'Reporting',
            items: [
                {label: 'GL to GL', to: '/reporting/gl-to-gl'},
                {label: 'GL to Account', to: '/reporting/gl-to-account'},
                {label: 'Account to Account', to: '/reporting/account-to-account'}
            ]
        },
        {
            label: 'Reporting Accounting',
            to: '/reporting/accounting'
        },
        {
            label: 'User Management',
            items: [
                {label: 'User', to: '/user-management/user'},
                {label: 'Role', to: '/user-management/role'},
            ]
        },
        {
            label: 'Parameter',
            items: [
                {label: 'GL Parameter Accounting', to: '/parameter/accounting'},
                {label: 'GL Parameter Exception', to: '/parameter/exception'},
                {label: 'General Parameter', to: '/parameter/general'},
                {label: 'Modul Management', to: '/parameter/modul-management'}
            ]
        },
    ];

    const logOutConfirm = useCallback(() => {
        confirmDialog({
            tagKey: confirmDialogKey.mainKey,
            message: 'Are you sure you want to logout?',
            closable: false,
            headerClassName: 'pt-0',
            icon: 'pi pi-info-circle',
            acceptClassName: 'p-button-text p-button-danger p-button-sm',
            acceptIcon: 'pi pi-power-off',
            acceptLabel: 'Logout',
            rejectIcon: 'pi pi-times',
            rejectLabel: 'Cancel',
            rejectClassName: 'p-button-text p-button-plain p-button-sm',
            accept() {
                nav('/login', {replace: true});
            }
        });
    }, [nav, confirmDialogKey]);

    const setProfileItemsDesktop = useCallback(() => {
        let items: any[] = [
            {
                label: 'Logout',
                icon: 'pi-power-off p-text-danger',
                command() {
                    logOutConfirm();
                }
            }
        ];
        setProfileItems(
          items
        );
    }, [setProfileItems, logOutConfirm]);

    const setProfileItemsMobile = useCallback(() => {
        setProfileItems(
            [
                {
                    label: 'Super Admin',
                    icon: 'pi-user'  
                },
                {
                    label: '232323232112',
                    icon: 'pi-user'  
                },
                {
                    label: 'Logout',
                    icon: 'pi-power-off',
                    command() {
                        logOutConfirm();
                    }
                }
            ]
        );
    }, [setProfileItems, logOutConfirm]);

    const getMainProfileItems = useCallback((tablet: any) => {
        if(tablet){
            setProfileItemsDesktop();
        }else{
            setProfileItemsMobile();  
        }
    }, [setProfileItemsDesktop, setProfileItemsMobile]);

    return {
        menu,
        setProfileItemsDesktop,
        setProfileItemsMobile,
        getMainProfileItems
    }
}