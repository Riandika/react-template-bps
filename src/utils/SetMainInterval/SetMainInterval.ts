import { useCallback, useEffect } from "react";
import { useMainRedux } from "../../config";

let notifTimer: any = null;

export default function SetMainInterval(){
    const { setNotifItems } = useMainRedux();

    const getMainInterval = useCallback(() => {
        let notifTime = 0;
        notifTimer = setInterval(() => {
            notifTime += 1;
            if(notifTime === 8){
                setNotifItems([
                    {
                        title: 'Erli.artika',
                        subTitle: '2 days ago',
                        label: 'Pending Approval - GL2GL',
                        command: () => {
                            
                        }
                    }
                ]);
            }

            if(notifTime === 15){
                clearInterval(notifTimer);

                setNotifItems([
                    {
                        title: 'Erli.artika',
                        subTitle: '2 days ago',
                        label: 'Pending Approval - GL2GL',
                        command: () => {
                            
                        }
                    },
                    {
                        title: 'User.app5',
                        subTitle: '5 days ago',
                        label: 'Pending Approval - GL2ACC',
                        command: () => {
                            
                        }
                    },
                    {
                        title: 'User.app3',
                        subTitle: '3 days ago',
                        label: 'Pending Approval - ACC2ACC',
                        command: () => {
                            
                        }
                    }
                ]);

            }
        }, 1000);

        
    }, [setNotifItems]);

    useEffect(() => {
        return () => {
            clearInterval(notifTimer);
            setNotifItems([]);
        }
    }, [setNotifItems]);

    return {
        getMainInterval
    }
}