import ScrollToTop from "./ScrollToTop/ScrollToTop";
import SetMainInterval from "./SetMainInterval/SetMainInterval";
import SetMainInitial from "./SetMainInitial/SetMainInitial";

export * from './format/index';

export {
    ScrollToTop,
    SetMainInterval,
    SetMainInitial
}