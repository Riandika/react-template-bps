import React, { useEffect } from "react";
import { useLocation, useNavigate, useParams } from 'react-router-dom';

export const withNavigation = (Component: any) => {
    return (props: any) => <Component {...props} location={useLocation()} params={useParams()} navigate={useNavigate()} />;
}

const ScrollToTop = ({ children, location: { pathname } }: any) => {
    useEffect(() => {
      window.scrollTo({
        top: 0,
        left: 0
      });
    }, [pathname]);

    return children || null;
  };

export default withNavigation(ScrollToTop);