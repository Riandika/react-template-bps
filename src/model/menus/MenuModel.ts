export interface MenuModel {
    label: string
    icon?: string
    to?: string
    items?: Array<MenuModel>
}

export interface AppMenuProps {
    model?: Array<MenuModel>
    root?: boolean
    onMenuItemClick?: (ev?: any) => void
}