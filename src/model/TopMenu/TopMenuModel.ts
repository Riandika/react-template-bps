export interface TopMenus {
    title?: string
    subTitle?: string
    label?: string
    icon?: string
    command?: (ev?: any) => void
}