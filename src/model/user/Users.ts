export interface Users {
    username?: string
    userName?: string
    email?: string
    branch?: any
    name?: string
    nip?: any
    groupName?: any
    unitName?: any
    position?: any
    role?: any
    cuid?: string
    status?: string
    lastLoginDate?: any
    lastLoginDateDashboard?: any
    disputeRole?: any
}