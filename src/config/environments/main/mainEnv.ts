export const configMain = {
    tabletSize: 540,
    desktopSize: 992,
    confirmDialogKey: {
        mainKey: 'main-confirm'
    },
    toastKey: {
        mainKey: 'main-toast',
        loginKey: 'login-toast'
    }
}

export function MainEnv(){
    const { desktopSize, tabletSize } = configMain;

    const isDesktop = () => {
        return window.innerWidth >= desktopSize;
    };
    
    const isTablet = () => {
        return window.innerWidth >= tabletSize;
    };

    return {
        isDesktop,
        isTablet
    }
}