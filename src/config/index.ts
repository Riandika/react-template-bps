import Routes from "./Routes/Routes";
import GlobalProvider from "./redux/globalProvider";

export * from './environments/index';
export * from './redux/action/index';

export {
    Routes,
    GlobalProvider
}

