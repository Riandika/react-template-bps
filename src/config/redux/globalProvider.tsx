import React, { ReactNode} from "react"
import { Provider } from "react-redux";
import { createStore } from "redux";
import reducer from "./reducer/reducer";

// type
type GlobalProviderProps = {
    children: ReactNode
}

const RootRedux = createStore(reducer);

function GlobalProvider({children}: GlobalProviderProps){
    return(
        <Provider store={RootRedux}>
            {children}
        </Provider>
    );
}

export default GlobalProvider;