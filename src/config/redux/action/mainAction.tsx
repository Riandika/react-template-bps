import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { TopMenus } from "../../../model";
import { MainEnv } from "../../environments";
import { ActionProps, ConfigAction, RootProps } from "../model";

let menuClick: boolean = false;

export function useMainRedux() {
    const dispatch = useDispatch()<ActionProps>;

    const {
        staticMenuInactive,
        ripple,
        notifMsgEmpty,
        notifItems,
        profileItems,
        overlayMenuActive,
        mobileMenuActive,
        layoutMode,
        isNotifTopBar,
        isProfileTopBar
    } = useSelector((state: RootProps) => state.mainReducer);

    const { isDesktop } = MainEnv();

    const setStaticMenuInactive = () => {
        dispatch({
            type: ConfigAction.setStaticMenuInactive,
            value: !staticMenuInactive
        });
    };

    const setNotifItems = useCallback((value: Array<TopMenus> = []) => {
        dispatch({
            type: ConfigAction.setNotifItems,
            value: value
        });
    }, [dispatch]);

    const setProfileItems = useCallback((value: Array<TopMenus>) => {
        dispatch({
            type: ConfigAction.setProfileItems,
            value: value
        });
    }, [dispatch]);

    const setOverlayMenuActive = (val?: boolean) => {
        dispatch({
            type: ConfigAction.setOverlayMenuActive,
            value: val !== undefined ? val : !overlayMenuActive
        })
    };

    const setMobileMenuActive = (val?: boolean) => {
        dispatch({
            type: ConfigAction.setMobileMenuActive,
            value: val !== undefined ? val : !mobileMenuActive
        });
    };

    const onMenuItemClick = (event: any) => {
        if(!event?.item?.items){
            setOverlayMenuActive(false);
            setMobileMenuActive(false);
        }
    };

    const setIsNotifTopBar = (val?: boolean) => {
        dispatch({
            type: ConfigAction.setIsNotifTopBar,
            value: val !== undefined ? val : !isNotifTopBar
        });
    };

    const setIsProfileTopBar = (val?: boolean) => {
        dispatch({
            type: ConfigAction.setIsProfileTopBar,
            value: val !== undefined ? val : !isProfileTopBar
        })
    };

    const onSidebarClick = () => {
        menuClick = true;
    };

    const onWrapperClick = () => {
        if (!menuClick) {
            setOverlayMenuActive(false);
            setMobileMenuActive(false);
            setIsNotifTopBar(false);
            setIsProfileTopBar(false);
        }

        menuClick = false;
    };

    const onToggleMenuClick = (event: any) => {
        menuClick = true;
        
        if (isDesktop()) {
            if (layoutMode === 'overlay') {
                if (mobileMenuActive === true) {
                    setOverlayMenuActive(true);
                }

                setOverlayMenuActive();
                setMobileMenuActive(false);
            }
            else if (layoutMode === 'static') {
                setStaticMenuInactive();
            }
        }
        else {
            setMobileMenuActive();
        }

        event.preventDefault();
    };

    const onNotificationClick = (event: any) => {
        menuClick = true;
        setIsProfileTopBar(false);
        setIsNotifTopBar();

        event.preventDefault();
    };

    const onProfileClick = (event: any) => {
        menuClick = true;
        setIsNotifTopBar(false);
        setIsProfileTopBar();

        event.preventDefault();
    };

    return {
        layoutMode,
        ripple,
        staticMenuInactive,
        notifMsgEmpty,
        notifItems,
        setNotifItems,
        profileItems,
        setProfileItems,
        overlayMenuActive,
        mobileMenuActive,
        onMenuItemClick,
        isNotifTopBar,
        isProfileTopBar,
        onSidebarClick,
        onWrapperClick,
        onToggleMenuClick,
        onNotificationClick,
        onProfileClick
    }
}