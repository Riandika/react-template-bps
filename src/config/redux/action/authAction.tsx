import { useSelector } from "react-redux"
import { RootProps } from "../model"

export function useAuthRedux(){
    const user = useSelector((state: RootProps) => state.authReducer);
    
    return {
        user
    } 
}