export const ConfigAction = {
    setStaticMenuInactive: 'SET_STATIC_MENU_INACTIVE',
    setNotifItems: 'SET_NOTIF_ITEMS',
    setProfileItems: 'SET_PROFILE_ITEMS',
    setOverlayMenuActive: 'SET_OVERLAY_MENU_ACTIVE',
    setMobileMenuActive: 'SET_MOBILE_MENU_ACTIVE',
    setIsNotifTopBar: 'SET_IS_NOTIF_TOPBAR',
    setIsProfileTopBar: 'SET_IS_PROFILE_TOPBAR'
}