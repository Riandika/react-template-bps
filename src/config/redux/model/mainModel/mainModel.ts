import { TopMenus } from "../../../../model";

export interface MainReduxProps {
    layoutMode: string
    staticMenuInactive: boolean
    ripple: boolean
    notifMsgEmpty: string
    notifItems: Array<TopMenus>
    profileItems: Array<TopMenus>
    overlayMenuActive: boolean
    mobileMenuActive: boolean
    isNotifTopBar: boolean
    isProfileTopBar: boolean
}