import { Users } from "../../../model";
import { MainReduxProps } from "./mainModel/mainModel";

export interface RootProps {
    mainReducer: MainReduxProps   
    authReducer: Users
}