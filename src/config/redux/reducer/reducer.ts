import { combineReducers } from "redux";
import mainReducer from "./mainReducer";
import authReducer from "./authReducer";

const reducer = combineReducers({
    mainReducer,
    authReducer
});

export default reducer;