import { ActionProps, ConfigAction, MainReduxProps } from "../model";

const initialState: MainReduxProps = {
    layoutMode: 'static',
    staticMenuInactive: false,
    ripple: true,
    notifMsgEmpty: 'Tidak ada notifikasi',
    notifItems: [],
    profileItems: [],
    overlayMenuActive: false,
    mobileMenuActive: false,
    isNotifTopBar: false,
    isProfileTopBar: false
}

const mainReducer = (state: MainReduxProps = initialState, action: ActionProps) => {
    const { 
        setStaticMenuInactive, 
        setNotifItems,
        setProfileItems,
        setOverlayMenuActive,
        setMobileMenuActive,
        setIsNotifTopBar,
        setIsProfileTopBar
    } = ConfigAction;

    if(action.type === setStaticMenuInactive){
        return state = {
            ...state,
            staticMenuInactive: action.value
        }
    }

    if(action.type === setNotifItems){
        return {
            ...state,
            notifItems: action.value
        }
    }

    if(action.type === setProfileItems){
        return {
            ...state,
            profileItems: action.value
        }
    }

    if(action.type === setOverlayMenuActive){
        return state = {
            ...state,
            overlayMenuActive: action.value
        }
    }

    if(action.type === setMobileMenuActive){
        return state = {
            ...state,
            mobileMenuActive: action.value
        }
    }

    if(action.type === setIsNotifTopBar){
        return state = {
            ...state,
            isNotifTopBar: action.value
        }
    }

    if(action.type === setIsProfileTopBar){
        return state = {
            ...state,
            isProfileTopBar: action.value
        }
    }

    return state;
}

export default mainReducer;