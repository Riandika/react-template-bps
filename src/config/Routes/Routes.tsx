import React, { useRef } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { Home, Login, Main } from '../../pages';
import { configMain } from '../environments';
import { Toast } from 'primereact/toast';

export default function RoutesCfg(){
    const toast = useRef({} as any);
    const { toastKey } = configMain;

    const showSuccess = () => {
        toast.current?.show({severity:'success', summary: 'Success', detail:'Berhasil Login', life: 3000, key: toastKey.loginKey});
    }

    return(
        <>
            <Toast ref={toast} key={toastKey.loginKey} />
            <Routes>
                <Route path='/login' element={<Login onSuccessLogin={showSuccess} />} />
                <Route path='/' element={<Main />}>
                    <Route path='/' element={<Navigate to="/home" />} />
                    <Route path='/home' element={<Home />} />
                    <Route path='/gl-to-gl' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/gl-to-account' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/account-to-account' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/approval/gl-to-gl' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/approval/gl-to-gl/:trxId' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/approval/gl-to-account' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/approval/gl-to-account/:trxId' element={<Login onSuccessLogin={showSuccess}/>} />
                    <Route path='/approval/account-to-account' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/approval/account-to-account/:trxId' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/approval/accounting' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/approval/accounting/:trxId' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/reporting/gl-to-gl' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/reporting/gl-to-account' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/reporting/account-to-account' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/reporting/accounting' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/user-management/user' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/user-management/role' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/parameter/accounting' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/parameter/exception' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/parameter/general' element={<Login onSuccessLogin={showSuccess} />} />
                    <Route path='/parameter/modul-management' element={<Login onSuccessLogin={showSuccess} />} />
                </Route>
            </Routes>
        </>
    );
}